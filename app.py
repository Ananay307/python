from __future__ import unicode_literals
from flask import Flask, jsonify, request
from flask_restful import Api, Resource
import os
import pymongo
import bcrypt
import youtube_dl
import random

from pip._vendor.urllib3.util import response

app = Flask(__name__)
api = Api(app)

uri = "mongodb://127.0.0.1:27017"
client = pymongo.MongoClient(uri)

db = client.SentencesDatabase
users = db["Users"]

class register(Resource):
    def post(self):
        postedData = request.get_json()

        username = postedData["Username"]
        password = postedData["Password"]

        hashed_pw = bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt())

        tag = db.Users.find({"Username": username}).count() == 0

        # if tag == False:
        #     ret1Json = {
        #         "Status": 303,
        #         "message": "Username already exists"
        #     }
        # return jsonify(ret1Json)

        users.insert({
            "Username": username,
            "Password": hashed_pw,
            "Sentence": "",
            "Tokens": 10
        })

        retJson = {
            "Status": 200,
            "message": "You are successfully signed up"
        }


        return jsonify(retJson)

def verifyPw(username, password):
    hashed_pw = users.find({
        "Username": username
    })[0]["Password"]

    if bcrypt.hashpw(password.encode('utf8'), hashed_pw) == hashed_pw:
        return True
    else:
        return False

def countTokens(username):
    tokens = users.find({
        "Username": username
    })[0]["Tokens"]
    return tokens

class Store(Resource):
    def post(self):
        postedData = request.get_json()

        if not request.args.get("Username"):
            retJson = {
                "Status": 302,
                "message": "Username is required"
            }
            return jsonify(retJson)
        elif not request.args.get("Password"):
            retJson = {
                "Status": 302,
                "message": "Password is required"
            }
            return jsonify(retJson)
        elif not postedData["Sentence"]:
            retJson = {
                "Status": 302,
                "message": "Sentence is required"
            }
            return jsonify(retJson)


        username = postedData["Username"]
        password = postedData["Password"]
        sentence = postedData["Sentence"]

        correct_pw = verifyPw(username, password)

        if not correct_pw:
            retJson = {
                "Status": 302,
                "Message": "You entered incorrect username/password"
            }
            return jsonify(retJson)

        num_tokens = countTokens(username)

        if num_tokens <= 0:
            retJson = {
                "Status": 301,
                "Token left": num_tokens,
                "Message": "You are out of token"
            }
            return jsonify(retJson)

        users.update({
            "Username": username
        }, {
            "$set":{
                "Sentence": sentence,
                "Tokens": num_tokens-1
            }
        })

        retJson = {
            "Status": 200,
            "Token left": num_tokens,
            "Message": "Sentence saved successfully"
        }
        return jsonify(retJson)

class Get(Resource):
    def post(self):
        postedData = request.get_json()

        if not postedData:
            retJson = {
                "Status": 302,
                "message": "Username is required"
            }
            return jsonify(retJson)

        username = postedData["Username"]
        password = postedData["Password"]

        correct_pw = verifyPw(username, password)

        if not correct_pw:
            retJson = {
                "Status": 302,
                "Message": "You entered incorrect username/password"
            }
            return jsonify(retJson)

        num_tokens = countTokens(username)

        if num_tokens <= 0:
            retJson = {
                "Status": 301,
                "Token left": num_tokens,
                "Message": "Buy premium pack as you are out of token"
            }
            return jsonify(retJson)

        users.update({
            "Username": username
        }, {
            "$set":{
                "Tokens": num_tokens-1
            }
        })

        sentence = users.find({
            "Username": username
        })[0]["Sentence"]

        retJson = {
            "Status": 200,
            "Sentence": sentence,
            "Token left": num_tokens
        }
        return jsonify(retJson)


class MyLogger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)

class Youtube(Resource):
    def post(self):
        postedData = request.get_json()

        if not postedData:
            retJson = {
                "Status": 302,
                "message": "The field is required"
            }
            return jsonify(retJson)

        link = postedData["Link"]

        ydl_opts = {
            'format': 'bestaudio/best',
            'outtmpl': '%(id)s',
            'noplaylist' : True,
            'logger': MyLogger()
        }
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            a = ydl.download([link])
            if a == 0:
                retJson = {
                    "Status": 200,
                    "Message": "Download complete"
                }
                return jsonify(retJson)
            else:
                retJson = {
                    "Status": 301,
                    "Message": "Download Interrupted"
                }


api.add_resource(register, '/register', '/reg')
api.add_resource(Store, '/store')
api.add_resource(Get, '/get')
api.add_resource(Youtube, '/youtube')

if __name__ == "__main__":
    app.run(debug=true)

from __future__ import unicode_literals
import os
import random
from flask import Flask, jsonify, request
from flask_restful import Api, Resource
import youtube_dl

app = Flask(__name__)
api = Api(app)


class MyLogger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)

class Youtube(Resource):
    def post(self):
        postedData = request.get_json()

        if not postedData:
            retJson = {
                "Status": 302,
                "message": "The field is required"
            }
            return jsonify(retJson)

        link = postedData["Link"]

        ydl_opts = {
            'format': 'bestaudio/best',
            'outtmpl': '%(id)s',
            'noplaylist' : True,
            'logger': MyLogger()
        }
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            a = ydl.download([link])
            if a == 0:
                retJson = {
                    "Status": 200,
                    "Message": "Download complete"
                }
                return jsonify(retJson)
            else:
                retJson = {
                    "Status": 301,
                    "Message": "Download Interrupted"
                }


api.add_resource(Youtube, '/youtube')

if __name__ == "__main__":
    app.run(debug=True)
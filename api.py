from flask import Flask, jsonify, request
from flask_restful import Api, Resource
import os
import pymongo

app = Flask(__name__)
api = Api(app)

uri = "mongodb://127.0.0.1:27017"
client = pymongo.MongoClient(uri)
db = client.aNewDB
UserNum = db["UserNum"]

UserNum.insert({
    'num_of_users': 0
})


class Visit(Resource):
    def get(self):
        prev_num = UserNum.find({})[0]['num_of_users']
        new_num = prev_num + 1
        UserNum.update({}, {"$set": {"num_of_users": new_num}})
        return str("Hello user:" + str(new_num))


def checkPostedData(postedData, functionName):
    if (functionName == "add" or functionName == "sub" or functionName == "mul"):
        if "x" not in postedData or "y" not in postedData:
            return 301
        else:
            return 200
    elif (functionName == "div"):
        if "x" not in postedData or "y" not in postedData:
            return 301
        elif int(postedData["y"]) == 0:
            return 302
        else:
            return 200


class Add(Resource):
    def post(self):
        postedData = request.get_json()

        status_code = checkPostedData(postedData, "add")
        if (status_code != 200):
            retJson = {
                'Message': "You are missing some parameters",
                'Status_Code': status_code
            }
            return jsonify(retJson)

        x = postedData["x"]
        y = postedData["y"]
        x = int(x)
        y = int(y)
        ret = x + y
        retmap = {
            'Message': ret,
            'Status_Code': 200
        }
        return jsonify(retmap)


class Subtract(Resource):
    def post(self):
        postedData = request.get_json()

        status_code = checkPostedData(postedData, "sub")
        if (status_code != 200):
            retJson = {
                'Message': "You are missing some parameters",
                'Status_Code': status_code
            }
            return jsonify(retJson)

        x = postedData["x"]
        y = postedData["y"]
        x = int(x)
        y = int(y)
        ret = x - y
        retmap = {
            'Message': ret,
            'Status_Code': 200
        }
        return jsonify(retmap)


class Multiply(Resource):
    def post(self):
        postedData = request.get_json()

        status_code = checkPostedData(postedData, "mul")
        if (status_code != 200):
            retJson = {
                'Message': "You are missing some parameters",
                'Status_Code': status_code
            }
            return jsonify(retJson)

        x = postedData["x"]
        y = postedData["y"]
        x = int(x)
        y = int(y)
        ret = x * y
        retmap = {
            'Message': ret,
            'Status_Code': 200
        }
        return jsonify(retmap)


class Division(Resource):
    def post(self):
        postedData = request.get_json()

        status_code = checkPostedData(postedData, "div")
        if (status_code == 302):
            retJson = {
                'Message': "The value of 'y' can not be 0",
                'Status_Code': status_code
            }
            return jsonify(retJson)
        if (status_code != 200):
            retJson = {
                'Message': "You are missing some parameters",
                'Status_Code': status_code
            }
            return jsonify(retJson)

        x = postedData["x"]
        y = postedData["y"]
        x = int(x)
        y = int(y)
        ret = (x * 1.0) / y
        retmap = {
            'Message': ret,
            'Status_Code': 200
        }
        return jsonify(retmap)


api.add_resource(Add, "/add")
api.add_resource(Subtract, "/subtract")
api.add_resource(Visit, "/hello")
api.add_resource(Multiply, "/multiply", "/multiplication", "/mul")
api.add_resource(Division, "/division", "/divide", "/div")

app.route('/')


def hello_world():
    return "Hello World!"
#
#
# if __name__ == "__main__":
#     app.run(debug=true)
# from flask import Flask, jsonify, request
# from flask_restful import Api, Resource
# import os
# import pymongo
# import bcrypt
#
# app = Flask(__name__)
# api = Api(app)
#
# uri = "mongodb://127.0.0.1:27017"
# client = pymongo.MongoClient(uri)
#
# db = client.SentencesDatabase
# users = db["Users"]
#
# class register(Resource):
#     def post(self):
#         postedData = request.get_json()
#
#         username = postedData["Username"]
#         password = postedData["Password"]
#
#         hashed_pw = bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt())
#
#         users.insert({
#             "Username": username,
#             "Password": hashed_pw,
#             "Sentence": "",
#             "Tokens": 7
#         })
#
#         retJson = {
#             "Status": 200,
#             "message": "You are successfully signed up"
#         }
#
#         return jsonify(retJson)
#
# def verifyPw(username, password):
#     hashed_pw = users.find({
#         "Username": username
#     })[0]["Password"]
#
#     if bcrypt.hashpw(password.encode('utf8'), hashed_pw) == hashed_pw:
#         return True
#     else:
#         return False
#
# def countTokens(username):
#     tokens = users.find({
#         "Username": username
#     })[0]["Tokens"]
#     return tokens
#
# class Store(Resource):
#     def post(self):
#         postedData = request.get_json()
#
#         username = postedData["Username"]
#         password = postedData["Password"]
#         sentence = postedData["Sentence"]
#
#         correct_pw = verifyPw(username, password)
#
#         if not correct_pw:
#             retJson = {
#                 "Status": 302
#             }
#             return jsonify(retJson)
#         num_tokens = countTokens(username)
#
#         if num_tokens <= 0:
#             retJson = {
#                 "Status": 301
#             }
#             return jsonify(retJson)
#
#         users.update({
#             "Username": username
#         }, {
#             "$set":{
#                 "Sentence": sentence,
#                 "Tokens": num_tokens-1
#             }
#         })
#
#         retJson = {
#             "Status": 200,
#             "Message": "Sentence saved successfully"
#         }
#         return jsonify(retJson)
#
#
# api.add_resource(register, '/register')
# api.add_resource(Store, '/store')
#
# if __name__ == "__main__":
#     app.run(debug=true)
